const mongoose = require("mongoose");
const mongooseDelete = require("mongoose-delete");

const BookingSchema = new mongoose.Schema(
  {
    movie: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "movie",
      required: true,
    },
    booking_list: [
      { date_booking: Date, time_booking: [mongoose.Schema.Types.Mixed] },
    ],
  },
  {
    timestamps: {
      createdAt: "createdAt",
      deletedAt: "deletedAt",
      updatedAt: "updatedAt",
    },
  }
);
//Enable Soft Delete
BookingSchema.plugin(mongooseDelete, { overridemethods: "all" });
module.exports = mongoose.model("booking", BookingSchema, "booking");
