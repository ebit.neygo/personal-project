const mongoose = require("mongoose");
const uri = process.env.MONGO_URI;
mongoose
  .connect(uri, {
    useUnifiedTopology: true, // Must be added
    useNewUrlParser: true, // Must be added
    // useCreateIndex: true, // Use to enable unique data type
    // useFindAndModify: false, // Use findOneAndUpdate instead of findAndModify
  })
  .then(() => {
    console.log("Mongoose connected to mongoDB");
  })
  .catch((error) => console.error(error));

const user = require("./user");
const movie = require("./movie");
const genres = require("./genres");
const tickets = require("./tickets");
module.exports = { user, genres, movie, tickets };
