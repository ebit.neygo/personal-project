const mongoose = require("mongoose");
const mongooseDelete = require("mongoose-delete");
const bcrypt = require("bcrypt");

//User Schema
const UserSchema = new mongoose.Schema(
  {
    name: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true, set: encryptPassword },
    point: { type: Number, required: false },
    tickets: {
      type: mongoose.Schema.Types.ObjectId,
      type: String,
      required: false,
      ref: "tickets",
    },
    foto_profile: {
      type: String,
      get: getUserFoto,
      required: false,
    },
  },
  {
    timestamps: {
      createdAt: "createdAt",
      deletedAt: "deletedAt",
      updatedAt: "updatedAt",
    },
    toJSON: { getters: true },
  }
);
function encryptPassword(password) {
  const encryptPassword = bcrypt.hashSync(password, 10);
  return encryptPassword;
}
function getUserFoto(foto_profile) {
  if (!foto_profile) return foto_profile;
  return `${process.env.S3_HOST}/${foto_profile}`;
}
//Enable Soft Delete
UserSchema.plugin(mongooseDelete, { overrideMethods: "all" });
module.exports = mongoose.model("user", UserSchema, "user");
