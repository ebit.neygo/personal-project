const mongoose = require("mongoose");
const mongooseDelete = require("mongoose-delete");

const TicketsSchema = new mongoose.Schema(
  {
    expired_status: { type: Boolean},
    movie: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "movie",
    },
    book_time: { type: [Date], required: true, ref: "movie" },
  },
  {
    timestamps: {
      createdAt: "createdAt",
      deletedAt: "deletedAt",
      updatedAt: "updatedAt",
    },
  }
);
//Enable Soft Delete
TicketsSchema.plugin(mongooseDelete, { overridemethods: "all" });
module.exports = mongoose.model("ticket", TicketsSchema, "ticket");
