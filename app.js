require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
});

//Express
const express = require("express");
const exFileUpload = require("express-fileupload");
const morgan = require("morgan");
const fs = require("fs");
const path = require("path");

//import all routes
const userRoutes = require("./routes/user_routes");
const movieRoutes = require("./routes/movie_routes");
const genresRoutes = require("./routes/genres_routes");
const ticketsRoutes = require("./routes/tickets_routes");
//error handlers
const errorsHandler = require("./middleware/errors_handler");


//import express app
const app = express();

//body parser to read body request
app.use(express.json());
app.use(
  express.urlencoded({
    //body support url encoded
    extended: true,
  })
);
if (process.env.NODE_ENV === "development") {
  app.use(morgan("dev"));
} else {
  // create a write stream (in append mode)
  let accessLogStream = fs.createWriteStream(
    path.join(__dirname, "access.log"),
    {
      flags: "a",
    }
  );

  // setup the logger
  app.use(morgan("combined", { stream: accessLogStream }));
}

//read request form data
app.use(exFileUpload());
app.use(express.static("public"));

//define app use specified routes
app.use("/user", userRoutes);
app.use("/movie", movieRoutes);
app.use("/genres", genresRoutes);
app.use("/ticket", ticketsRoutes);

// app use error handler
app.use(errorsHandler);

//server run
if (process.env.NODE_ENV !== "test") {
  app.listen(3000, () => console.log("Server Running on port 3000"));
}

module.exports = app;
