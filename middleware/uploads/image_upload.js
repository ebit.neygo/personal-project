const { sendToS3 } = require("./agent_s3");

exports.sendingPoster = async (req, res, next) => {
  try {
    if (req.files) {
      const file = req.files.poster;
      req.body.poster = await sendToS3(req.body.directory, file);
    }
    next();
  } catch (e) {
    return res.status(500).json({
      message: e.message,
    });
  }
};
exports.sendingProfileImage = async (req, res, next) => {
  try {
    if (req.files) {
      const file = req.files.foto_profile;
      req.body.foto_profile = await sendToS3(req.body.directory, file);
    }
    next();
  } catch (e) {
    return res.status(500).json({
      message: `send ` + e.message,
    });
  }
};
