const validator = require("validator");
const connection = require("../../models");

exports.create = async (req, res, next) => {
  try {
    req.body.directory = "Movie-Poster";

    next();
  } catch (error) {
    return res.status(500).json({
      message: "Internal Server Error",
      error: error,
    });
  }
};
exports.update = async (req, res, next) => {
  try {
    req.body.directory = "Movie-Poster";
    next();
  } catch (error) {
    return res.status(500).json({
      message: "Internal Server Error",
      error: error,
    });
  }
};
