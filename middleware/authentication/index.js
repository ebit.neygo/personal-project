const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const bcrypt = require("bcrypt");
const JWTStrategy = require("passport-jwt").Strategy;
const ExtractJWT = require("passport-jwt").ExtractJwt;
const { user } = require("../../models");

//Signup Block=========================================
exports.signup = (req, res, next) => {
  passport.authenticate("signup", { session: false }, (err, user, info) => {
    if (err) {
      return next(err);
    }
    if (!user) {
      return next({ message: info.message, statusCode: 401 });
    }
    req.user = user;
    next();
  })(req, res, next);
};
//Signup Strategy===
passport.use(
  "signup",
  new LocalStrategy(
    {
      usernameField: "email",
      passwordField: "password",
      passReqToCallback: true,
    },
    async (req, email, password, done) => {
      try {
        let userSignup = await user.create(req.body);
        return done(null, userSignup, {
          message: "Success",
        });
      } catch (e) {
        return done(null, false, {
          message: e.message,
        });
      }
    }
  )
);
//end of Signup Block
exports.login = (req, res, next) => {
  passport.authenticate("login", { session: false }, (err, user, info) => {
    if (err) {
      return next(err);
    }
    if (!user) {
      return next({ message: info.message, statusCode: 401 });
    }
    req.user = user;
    next();
  })(req, res, next);
};
passport.use(
  "login",
  new LocalStrategy(
    {
      usernameField: "email",
      passwordField: "password",
      passReqToCallback: true,
    },
    async (req, email, password, done) => {
      try {
        let userLogin = await user.findOne({ email: req.body.email });
        return done(null, userLogin, {
          message: "Success",
          userLogin,
        });
      } catch (e) {
        return done(null, false, {
          message: e.message,
        });
      }
    }
  )
);
exports.authUser = (req, res, next) => {
  passport.authorize("authUser", (err, user, info) => {
    if (err) {
      return next(err);
    }
    if (!user) {
      return next({ message: info.message, statusCode: 403 });
    }
    req.user = user;
    next();
  })(req, res, next);
};
passport.use(
  "authUser",
  new JWTStrategy(
    {
      secretOrKey: process.env.JWT_SECRET,
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
    },
    async (token, done) => {
      try {
        // console.log(token);
        let authorizeUser = await user.findOne({
          _id: token.user.id,
        });
        if (authorizeUser) return done(null, token.user);
      } catch (error) {
        return next(error);
      }
    }
  )
);
