const jwt = require("jsonwebtoken")

class AuthController {
    async getToken(req, res, next) {
        try {
            const body = {
                user: {
                    id: req.user.id
                }
            }
            const token = jwt.sign(body, process.env.JWT_SECRET, {
                expiresIn: "30d"
            })
            return res.status(200).json({
                message: "Success",
                token
            })
        } catch (error) {
            return next(error);
        }
    }
}
module.exports = new AuthController();