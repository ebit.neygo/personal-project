const crypto = require("crypto");
const path = require("path");
const { movie, genres } = require("../models");
const moment = require("moment");

class MovieController {
  async create(req, res, next) {
    try {
      const duplicationCheck = await movie.findOne({ title: req.body.title });
      if (duplicationCheck == null) {
        let userData = await movie.create(req.body);
        let data = await movie.findOne({ _id: userData._id });
        return res.status(201).json({
          message: "Success",
          data,
        });
      } else {
        return next({
          message: `${duplicationCheck.title} is already in our database`,
          statusCode: 409,
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: error.message,
      });
    }
  }
  async showMovie(req, res, next) {
    try {
      await movie
        .findOne({ _id: req.params.id })
        .populate("movie_genre")
        .exec(function (error, result) {
          if (error) return next(error);
          return res.status(200).json({
            message: "Success",
            result,
          });
        });
    } catch (error) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: error.message,
      });
    }
  }
  async showMovies(req, res) {
    try {
      await movie
        .find({})
        .populate("movie_genre")
        .exec(function (error, moviesData) {
          if (error) return next(error);
          return res.status(200).json({
            message: "Success",
            moviesData,
          });
        });
    } catch (error) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: error.message,
      });
    }
  }
  async sortMoviesByRelease(req, res, next) {
    try {
      let data = await movie
        .find({})
        .populate("movie_genre")
        .sort({ createdAt: "asc" });
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (error) {
      return next(error);
    }
  }
  async searchMovie(req, res, next) {
    try {
      const sortParam = req.body.sort_key;
      let sortVar = {};
      sortVar[sortParam] = 'desc';
      let data = await movie
        .find({ title: { $regex: req.body.keywords } })
        .populate("movie_genre")
        .sort(sortVar);
      if (data.length <= 0) {
        return res.status(404).json({
          message: `No Movie found with title ${req.body.keywords}`,
        });
      }
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (error) {
      return next(error);
    }
  }
  async updateMovie(req, res, next) {
    try {
      let data = await movie.findOneAndUpdate(
        {
          _id: req.params.id,
        },
        req.body,
        {
          new: true,
        }
      );
      return res.status(201).json({
        message: "Success",
        data,
      });
    } catch (error) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: error.message,
      });
    }
  }
  async deleteMovie(req, res) {
    try {
      await movie.deleteOne({ _id: req.params.id });
      return res.status(200).json({
        message: "Success",
      });
    } catch (error) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: error.message,
      });
    }
  }
}

module.exports = new MovieController();
