const crypto = require("crypto");
const path = require("path");
const { genres } = require("../models");

class GenresController {
  async create(req, res) {
    try {
      let checkGenre = await genres.findOne({
        genreName: req.body.genreName,
      });
      if (checkGenre) {
        return res.status(403).json({
          message: `${req.body.genreName} already in database`,
          checkGenre,
        });
      } else {
        let userData = await genres.create(req.body);
        let data = await genres.findOne({ _id: userData._id });
        return res.status(201).json({
          message: "Success",
          data,
        });
      }
    } catch (error) {
      console.error(error);
      return res.status(500).json({
        message: "Internal Server Error",
        error: error.message,
      });
    }
  }
}

module.exports = new GenresController();
