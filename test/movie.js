module.exports = userTest = () => {
  const request = require("supertest"); // Import supertest
  const app = require("../app"); // Import app
  const { movie } = require("../models"); // Import user and transaksi models


  beforeAll(async () => {
    const mongoose = require("mongoose")
    const uri = process.env.MONGO_URI;
    await mongoose.connect(uri, { useNewUrlParser: true })
    await movie.deleteMany()
  });
  //Get 1 Movie Test
  describe("Post Movie", () => {
    it("create movie", async () => {
      const res = await request(app).post("/movie").field({
        title: "Mortal Combat TEST",
        description:
          "MMA fighter Cole Young seeks out Earth's greatest champions in order to stand against the enemies of Outworld in a high stakes battle for the universe.",
        movie_genre: "61308e7ef606b26f4a36e3ab",
        poster: "kosong",
      }).attach("poster", "test/bliss.jpeg");
      expect(res.statusCode).toEqual(201);
      expect(res.body.message).toEqual("Success");
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body).toHaveProperty("data");
    });
  });

  describe("Get Movies", () => {
    it("should get  movies", async () => {
      const res = await request(app).get(`/movie`);;
      expect(res.statusCode).toEqual(200);
      expect(res.body.message).toEqual("Success");
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body).toHaveProperty("moviesData");
      expect(res.body.moviesData[0]).toHaveProperty("poster")
    });
  });
};
