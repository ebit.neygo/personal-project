const express = require("express");
const router = express.Router();

//Middleware
const userValidator = require("../middleware/validators/user_validator");
const auth = require("../middleware/authentication/index");

//Controller
const userController = require("../controllers/user_controller");
const authController = require("../controllers/authentication_controller");

router.post(
  "/signup",
  userValidator.signup,
  auth.signup,
  authController.getToken
);
router.post("/signin", auth.login, authController.getToken);
router.post("/booking", auth.authUser, userController.bookTheMovie);
router.get("/:id", auth.authUser, userController.getUserInfo);
module.exports = router;
