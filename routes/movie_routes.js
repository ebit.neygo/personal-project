const express = require("express");
const router = express.Router();

//Middleware
const movieValidator = require("../middleware/validators/movie_validator");
const { sendingPoster } = require("../middleware/uploads/image_upload");

//Controller
const movieController = require("../controllers/movie_controller");

router.post("/", movieValidator.create, sendingPoster, movieController.create);
router.get("/show/:id", movieController.showMovie);
router.get("/", movieController.showMovies);
router.get("/sort", movieController.sortMoviesByRelease);
router.get("/search", movieController.searchMovie);
router.put(
  "/update/:id",
  movieValidator.update,
  sendingPoster,
  movieController.updateMovie
);
router.delete("/delete/:id", movieController.deleteMovie);
module.exports = router;
